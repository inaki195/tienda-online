from pyexpat import model
from django import forms
from .models import Usuario
from apps.users import models


geeks_CHOICE=(
    ("1","cliente"),
    ("2","distribuidor")
)

class UsuarioForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model=Usuario
        fields=('nombre','apellido','nickname','email','password','distribuidor')
