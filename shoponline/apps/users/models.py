from operator import mod
from django.db import models

# Create your models here.

class Usuario(models.Model):
    nombre=models.CharField(max_length=100,null=False)
    apellido=models.CharField(max_length=200)
    nickname=models.CharField(max_length=100)
    email=models.EmailField(max_length=200)
    password=models.CharField(max_length=300)
    fecha_alta=models.DateTimeField( auto_now_add=True)
    distribuidor=models.BooleanField()
    foto_perfil=models.ImageField()

class Cliente(models.Model):
    monedero=models.DecimalField(decimal_places=2,max_digits=5)
    juegos=models.TextField()
    id_usuario=models.OneToOneField(Usuario,on_delete=models.CASCADE)

class Proveedor(models.Model):
    id_usuario=models.OneToOneField(Usuario,on_delete=models.CASCADE)
